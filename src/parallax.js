const parallax = (el, binding) => {

    let parentElement = el.parentElement;
    parentElement.classList.add('parallax');

    if (!binding.value) binding.value = {};

    let options = {
        start: 0,
        end: 0,
        offset: 0.5,
        direction: 'down',
        ...binding.value
    }

    let sideways = options.direction === 'left' || options.direction === 'right',
        elTop, elBottom, offset;

    el.parallaxInit = () => {

        // Element boundaries relative to document
        let container = parentElement.getBoundingClientRect();
        elTop = container.top + window.scrollY;
        elBottom = elTop + container.height;

        //Offset image on opposite axis
        if (sideways) {
            offset = -(el.offsetHeight - container.height) * options.offset;
        } else {
            offset = -(el.offsetWidth - container.width) * options.offset;
        }

    }


    el.parallax = () => {

        //Return early if image is not in view
        if ((elTop - window.innerHeight - window.scrollY) > 0 || elBottom - window.scrollY < 0) return;

        let container = parentElement.getBoundingClientRect();

        let size = sideways ? container.width : container.height;

        let containerScrollPercent = (window.innerHeight - container.top) / (window.innerHeight + size);

        if (options.direction === 'down' || options.direction === 'right') {
            containerScrollPercent = 1 - containerScrollPercent;
        }

        let elSize = sideways ? el.offsetWidth : el.offsetHeight;
        
        let offsetStart = options.start * elSize;
        let offsetEnd = options.end * elSize;

        elSize = elSize - offsetStart - offsetEnd;

        let scroll = (-(elSize - size) * containerScrollPercent) - offsetStart;

        let transform = sideways ? `translate3d(${scroll}px,${offset}px,0)` : `translate3d(${offset}px,${scroll}px,0)`;

        el.style.transform = transform;

    }

    window.addEventListener('parallax-resize', () => {
        el.parallaxInit();
        el.parallax();
    }, false);
    window.addEventListener('parallax-scroll', el.parallax, false);

    el.parallaxInit();
    el.parallax();

    if (el.nodeName === 'IMG') {
        el.onload = () => {
            el.parallaxInit();
            el.parallax();
        }
    }

}

export default parallax;