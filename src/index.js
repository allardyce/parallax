import './parallax.css'
import parallax from './parallax'

// https://developer.mozilla.org/en-US/docs/Web/Events/resize
const initParallaxResizeEvent = () => {

    const throttle = (type, name, obj) => {

      obj = obj || window;

      let running = false;

      const func = () => {

            if (running) {
                console.log('blocked');
                return;
            }

            running = true;

            requestAnimationFrame(() => {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });

        };

        obj.addEventListener(type, func);

    };
  
    throttle('resize', 'parallax-resize');
    throttle('scroll', 'parallax-scroll');

}
initParallaxResizeEvent();
  

const reset = (el) => {
    if (el.parallax) {
        window.removeEventListener('parallax-scroll', el.parallax);
        window.removeEventListener('parallax-resize', el.parallax);
    }
}

const plugin = {

    install(Vue, os = {}) {

        Vue.directive('parallax', {

            bind(el, binding) {

                el.classList.add('parallax-item');

                if (binding.value && (binding.value.direction === 'left' || binding.value.direction === 'right')) {
                    el.classList.add('parallax-sideways');
                }

            },

            inserted(el, binding) {

                reset(el);

                parallax(el, binding);

            },

            update(el, binding) {

                reset(el);
                
                parallax(el, binding);
                
            },

            unbind(el) {

                reset(el);

            }

        });

    }

}

export default plugin;
