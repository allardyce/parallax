import Vue from 'vue'

import App from './App.vue'

import './main.scss'

Vue.config.productionTip = false

import parallax from '../../'
Vue.use(parallax);

new Vue({
  render: h => h(App),
}).$mount('#app')
