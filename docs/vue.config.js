module.exports = {
    
    devServer: {
        host: '0.0.0.0'
    },

    publicPath: process.env.NODE_ENV === 'production' ? '/parallax/' : '/',

    chainWebpack: (config) => {
        const svgRule = config.module.rule('svg');
     
        svgRule.uses.clear();
     
        svgRule
          .use('vue-svg-loader')
          .loader('vue-svg-loader');
    },

}