# @allardyce/parallax

A Vue Directive Parallax Solution

Live Demo: [https://allardyce.gitlab.io/parallax/](https://allardyce.gitlab.io/parallax/)

## Install

```
yarn add @allardyce/parallax
```


## Setup

```
import parallax from '@allardyce/parallax'
Vue.use(parallax)
```

## Usage

#### Default Markup with *v-parallax* directive
```
<section class="min">
    <img src="/valid/path/image.jpg" v-parallax />
</section>
```

#### Markup with *v-parallax* directive and additional options
```
<section class="min">
    <img src="/valid/path/image.jpg" v-parallax="{ direction: 'up', start: 0.2, end: 0.1 }" />
</section>
```


|Option|Default|Values|Description|
|--- |--- |--- |--- |
|**direction**|'down'|'down', 'up', 'left', 'right'|The direction the element moves when scrolling.|
|**start**|0|0 to 1|Decimal giving the percentage of the element to start at. EG: 0.2 = crop top 20% of image.|
|**end**|0|0 to 1|Decimal giving the percentage of the element to end at. EG: 0.1 = crop bottom  10% of image.|

> **Tip**: You can use start and end to change the speed of the parallax scrolling.

> **Tip**: Adding the *min* class to the parent gives a `min-height: 30rem;` styling.


## Development

Run two terminals

`yarn watch`

`cd docs && yarn serve`