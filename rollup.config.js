import babel from 'rollup-plugin-babel'
import postcss from 'rollup-plugin-postcss'
import filesize from 'rollup-plugin-filesize';

import pkg from './package.json';

export default {

    input: 'src/index.js',

    output: [
        {
            file: pkg.main,
            format: 'cjs',
            exports: 'named'
        },
        {
            file: pkg.module,
            format: 'es'
        },
        {
            file: 'dist/allardyce-parallax.js',
            format: 'umd',
            name: 'parallax',
            exports: 'named',
            globals: {
                vue: 'Vue',
            }
        }
    ],

    plugins: [

        postcss(),

        babel(),

        filesize(),

    ],

  };