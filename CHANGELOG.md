# Changelog

## [0.1.3] - 2018-03-17
### Changed
- Added offset option
- Smarter calculating of initial variables
- Deployment improvements

## [0.1.2] - 2018-03-16
### Changed
- Optimised repainting to request frame animation

## [0.1.1] - 2018-03-03
### Changed
- Updated README to include demo site link
- Changed min height of container to use `rem` instead of `vw`

## [0.1.0] - 2018-03-03
### Added
- Intial publish